// --------------------------------------------------------
// Code generated by Papyrus Java
// --------------------------------------------------------

package ito.poo.clase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/************************************************************/
/**
 * 
 */
public class canciones {
	public canciones(String titulo, int duracionMin, int duracionSeg, String[] solInter, LocalDate fechaReg,
			LocalDate fechaEstreno) {
		super();
		this.Titulo = Titulo;
		this.DuracionMin = duracionMin;
		this.DuracionSeg = duracionSeg;
		this.SolInter = SolInter;
		this.FechaReg = fechaReg;
		FechaEstreno = fechaEstreno;
	}

	public canciones() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	private String Titulo;
	private int DuracionMin;
	private int DuracionSeg;
	private HashSet<String> SolInter=new HashSet<String>();
	private java.time.LocalDate FechaReg;
	private java.time.LocalDate FechaEstreno;

	/***/
	public boolean addInterprete(String nombre) {
		boolean addInterprete=false;
		
		addInterprete=this.SolInter.add(nombre);
		return addInterprete;
	}

	

	public String getTitulo() {
		return this.Titulo;
	}

	public void setTitulo(String Titulo) {
		Titulo = Titulo;
	}

	public int getDuracionMin() {
		return DuracionMin;
	}

	public void setDuracionMin(int duracionMin) {
		DuracionMin = duracionMin;
	}

	public int getDuracionSeg() {
		return DuracionSeg;
	}

	public void setDuracionSeg(int duracionSeg) {
		DuracionSeg = duracionSeg;
	}

	public String[] getSolInter() {
		return this.getSolInter();
	}

	public void setSolInter(String[] SolInter) {
		SolInter = SolInter;
	}

	public java.time.LocalDate getFechaReg() {
		return FechaReg;
	}

	public void setFechaReg(java.time.LocalDate fechaReg) {
		FechaReg = fechaReg;
	}

	public java.time.LocalDate getFechaEstreno() {
		return FechaEstreno;
	}

	public void setFechaEstreno(java.time.LocalDate fechaEstreno) {
		FechaEstreno = fechaEstreno;
	}
	@Override
	public String toString() {
		return "canciones [Titulo=" + Titulo + ", DuracionMin=" + DuracionMin + ", DuracionSeg=" + DuracionSeg
				+ ", SolInter=" + SolInter + ", FechaReg=" + FechaReg + ", FechaEstreno="
				+ FechaEstreno + "]";
	}
}
